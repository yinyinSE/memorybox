import React, { Component } from 'react';
import {
  AppRegistry
} from 'react-native';
import MemoryBoxApp from './src/components/MemoryBoxApp';

export default class MemoryBox extends Component {
  render() {
    return (
      <MemoryBoxApp />
    );
  }
}

AppRegistry.registerComponent('MemoryBox', () => MemoryBox);
