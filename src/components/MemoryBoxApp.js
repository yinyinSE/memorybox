import React, { Component } from 'react';
import { Navigator, Text, StyleSheet } from 'react-native';
import Starting from './Starting';

export default class MemorBoxApp extends Component {

	render() {
		  return (
		    <Navigator 
		      initialRoute={{root: true, title: 'Memory box', component: Starting}}
		      //renderScene = {(route, Navigator) => {
		      	//return <Starting />
		      //}}

	      		renderScene={(route, navigator) => {
         
         	 	const props = Object.assign(route.props || {}, {navigator})
          		return React.createElement(route.component, props)
        }}
		    >
		    </Navigator>
		  )
	}
}
