import React, { Component } from 'react';
import { View, Text, TouchableHighlight, StyleSheet} from 'react-native';
import List from './List';

export default class Starting extends Component {
	startPress() {
		this.props.navigator.push({component: List})
	}

	render() {
		return (
		<View style={styles.container}>
			<Text style={styles.headerText}>Memory box app</Text>
			<Text style={styles.contentText}>Write down your feelings or important words directly</Text>
			<TouchableHighlight onPress={this.startPress.bind(this)} style={styles.button}>
				<Text style={styles.buttonText}>Start</Text>
			</TouchableHighlight>
		</View>
		)
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
    	alignItems: "center",
    	justifyContent: "center",
    	backgroundColor: "rgb(234,234,234)"
	},
	headerText: {
		textAlign: 'center',
		fontSize: 25
	},
	contentText: {
		textAlign: 'center',
		padding: 20
	},
	button: {
		marginTop: 30,
		backgroundColor: "rgb(221,60,128)",
		borderRadius: 50,
		width: 100,
		padding: 20
	},
	buttonText: {
		color: 'rgb(255,255,255)',
		textAlign: 'center',
		fontSize: 17
	}
})
